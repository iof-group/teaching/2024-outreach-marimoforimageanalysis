import marimo

__generated_with = "0.8.15"
app = marimo.App(width="medium")


@app.cell
def __(mo):
    mo.md(
        """
        # The Data
        ## Some information
        [**H2B-mCherry (red) + mEGFP-α-Tubulin (green)**](https://cellcognition-project.org/demo_data.html)

        Human Hela Kyoto cells stably expressing fluorescent markers of chromatin (histone-2b, H2B) and alpha-Tubulin. Data acquired by Katja Beck (Kutay group, ETH Zurich) and Michael Held (Gerlich group, ETH Zurich) at a Molecular Devices ImageXpress Micro.

        Widefield epifluorescence, 20x dry objective (0.3225 µm / pixel), 1392x1040 pixel, 206 frames x 4.6 min = 15.8 h
        """
    )
    return


@app.cell
def __(mo):
    frame_selection_slider = mo.ui.range_slider(start=1, stop=206, show_value=True, label='Frames (start/end)')
    channel_selection = mo.ui.dropdown(options=['rfp', 'gfp'], value='rfp', label='Channel')
    position_selection = mo.ui.dropdown(options=['13', '37', '46'], value='13', label='Position')
    load_button = mo.ui.run_button(label='Load Images')

    mo.md(
        f"""
        ## Load images
        {mo.vstack([
            mo.hstack([frame_selection_slider, channel_selection, position_selection], justify='space-around'),
            load_button.center()
        ])}
        """
    )
    return (
        channel_selection,
        frame_selection_slider,
        load_button,
        position_selection,
    )


@app.cell
def __(
    channel_selection,
    frame_selection_slider,
    get_position,
    load_button,
    mo,
    position_selection,
):
    mo.stop(not load_button.value)

    img = get_position(
        './data/', 
        position_selection.value,
        channel_selection.value,
        frame_selection_slider.value
    )
    return img,


@app.cell
def __(img, mo):
    display_slider = mo.ui.slider(start=0, stop=img.shape[0]-1, label='Frame to display')

    mo.md(
        f"""
        ## Display Image
        {display_slider}
        """
    )
    return display_slider,


@app.cell
def __(display_slider, img, show_image):
    show_image(img, display_slider.value)
    return


@app.cell
def __(img, mo):
    adaptive_threshold = mo.ui.number(start=1, stop=10, value=2, label='Adaptive threshold')
    adaptive_sigma = mo.ui.number(start=1, stop=50, value=1, label='Adaptive sigma')
    adaptive_sigma_bkgr = mo.ui.number(start=1, stop=50, value=21, label='Adaptive sigma bkgr')
    watershed_sigma = mo.ui.number(start=1, stop=20, value=8, label='Watershed sigma')
    object_min_size = mo.ui.number(start=1, stop=10000, value=512, label='Object min size')

    selected_frame = mo.ui.slider(start=0, stop=img.shape[0]-1, value=int(img.shape[0]/2), show_value=True, label='Frame to segment')

    mo.md(
        f"""
        # The Segmentation
        ## The Pipeline
        1. Local adaptive thresholding
        2. Fill holes
        3. Watershed split merged objects
        4. Remove objects that are too small (probably not nuclei)

        ## Parameter optimization
        Play around with the sliders below to find optimal segmentation.

        {mo.vstack([
        mo.hstack([
            adaptive_threshold,
            adaptive_sigma,
            adaptive_sigma_bkgr]),    
        mo.hstack([watershed_sigma,object_min_size, selected_frame])    
        ])}
        """
    )
    return (
        adaptive_sigma,
        adaptive_sigma_bkgr,
        adaptive_threshold,
        object_min_size,
        selected_frame,
        watershed_sigma,
    )


@app.cell
def __(
    adaptive_sigma,
    adaptive_sigma_bkgr,
    adaptive_threshold,
    img,
    object_min_size,
    segment_nuclei,
    selected_frame,
    watershed_sigma,
):
    img_test = img[selected_frame.value]

    res = segment_nuclei(
        img_test,
        adaptive_threshold=adaptive_threshold.value,
        adaptive_sigma=adaptive_sigma.value,
        adaptive_sigma_bkgr=adaptive_sigma_bkgr.value,
        watershed_sigma=watershed_sigma.value,
        object_min_size=object_min_size.value,
    )
    return img_test, res


@app.cell
def __(img_test, res, show_segmentation):
    show_segmentation(img_test, res)
    return


@app.cell
def __(
    adaptive_sigma,
    adaptive_sigma_bkgr,
    adaptive_threshold,
    img,
    mo,
    object_min_size,
    watershed_sigma,
):
    dict_params = {
        'Adaptive threshold': adaptive_threshold.value,
        'Adaptive sigma': adaptive_sigma.value,
        'Adaptive sigma bkgr': adaptive_sigma_bkgr.value,
        'Watershed sigma': watershed_sigma.value,
        'Object min size': object_min_size.value
    }

    analyze_button = mo.ui.run_button(label='Analyze Frames')
    segmentation_display_frame = mo.ui.slider(start=0, stop=img.shape[0]-1, value=int(img.shape[0]/2), show_value=True, label='Frame to display')

    mo.md(
        f"""
        ## Batch segment

        Confirm selected parameters and press the button.

        {mo.ui.table(dict_params, selection='single', label='Selected Parameters')}

        {analyze_button}
        """
    )
    return analyze_button, dict_params, segmentation_display_frame


@app.cell
def __(analyze_button, dict_params, img, mo, segment_nuclei_movie):
    mo.stop(not analyze_button.value)

    labels_movie = segment_nuclei_movie(img, dict_params)
    return labels_movie,


@app.cell
def __(analyze_button, mo):
    mo.stop(not analyze_button.value)

    export_form = mo.md('''
        Selected directory to export results of segmentation.

        {filename}

        {output_directory}
    ''').batch(
            filename = mo.ui.text(label='File name'), 
            output_directory = mo.ui.file_browser(multiple=False, selection_mode='directory', label='Select output directory')
    ).form()

    export_form
    return export_form,


@app.cell
def __(export_form, labels_movie, mo, os, tifffile):
    mo.stop(not export_form.value, mo.md("Submit the form to export results"))

    tifffile.imwrite(
        os.path.join(export_form.value['output_directory'][0].path, export_form.value['filename'] + '.tif'), 
        labels_movie
    )


    mo.md("Data Exported!").callout(kind='success').center()
    return


@app.cell
def __(
    binary_fill_holes,
    distance_transform_edt,
    feature,
    filters,
    measure,
    mo,
    morphology,
    np,
    plt,
    segmentation,
    tifffile,
):
    def show_segmentation(img, labels):
        f, axes = plt.subplots(1, 2, figsize=(12,4))
        axes[0].imshow(
            img,
            cmap='gray', 
            vmin=np.percentile(img, 1), 
            vmax=np.percentile(img, 99.5)
        )
        axes[1].imshow(labels)

        for ax in axes:
            ax.axis('off')
        return f

    def show_image(img_series, frame):
        f, ax = plt.subplots(1)
        ax.imshow(
            img_series[frame], 
            cmap='gray', 
            vmin=np.percentile(img_series[frame], 1), 
            vmax=np.percentile(img_series[frame], 99.5)
        )
        ax.axis('off')
        ax.set_title('Frame {}'.format(frame+1))
        return ax

    def get_position(data_dir, pos_id, channel, t_range):
        """Read all images from *data_dir* for a specified *pos_id*, *channel* and time interval (*t_start*, *t_end*)"""
        mov = []
        for t in mo.status.progress_bar(range(*t_range), title=f"Load Position 00{pos_id}, channel {channel}"):
            img_t = tifffile.imread(
                f"{data_dir}/00{pos_id}/tubulin_P00{pos_id}_T00{t:03d}_C{channel}_Z1_S1.tif"
            )
            mov.append(img_t)

        return np.stack(mov)

    def local_adaptive_thresholding(
        img,
        threshold_adaptive,
        sigma,
        sigma_bkgr,
    ):
        """Function to perform a simple local adaptive thresholding algorithm"""
        img_blur = filters.gaussian(img, sigma=sigma, preserve_range=True)
        img_bkgr = filters.gaussian(img, sigma=sigma_bkgr, preserve_range=True)

        img_adapt = img_blur - img_bkgr

        # Threshold background subtracted image instead of original image
        return img_adapt > threshold_adaptive

    def watershed_split(img_seg, sigma):
        """Function to perform watershed object splitting of *img_seg*"""

        # calculate euclidean distance transform of binary mask
        img_edt = distance_transform_edt(img_seg > 0)

        # blur the edt image to make peak finding easier
        img_edt_blur = filters.gaussian(img_edt, sigma=sigma, preserve_range=True)

        # Find seeds for watershed by finding maximum distance pixels (max distance from edge)
        # Try different options for the seeded watershed
        peaks = feature.peak_local_max(img_edt_blur, min_distance=5, labels=img_seg)

        # create empty image of the same shape as original image
        seeds = np.zeros_like(img_seg, dtype=bool)
        # populate empty image with peaks (True=peak, False=not a peak)
        for y_peak, x_peak in peaks:
            seeds[y_peak, x_peak] = True
        # convert peak image into a label objects where every object has a unique index to create seed image
        seeds = measure.label(seeds)

        # invert distance transform to find valleys instead of mountains
        distance = -img_edt_blur

        # perform the watershed segmentation
        seg_watershed = segmentation.watershed(distance, markers=seeds, mask=img_seg > 0)
        return seg_watershed

    def segment_nuclei(
        img,
        adaptive_threshold,
        adaptive_sigma,
        adaptive_sigma_bkgr,
        watershed_sigma,
        object_min_size,
    ):
        """
        Function to perform complete segmentation pipeline:
        1. Local adaptive thresholding
        2. Fill holes
        3. Watershed split merged objects
        4. Remove objects that are too small (probably not nuclei)
        """

        # local adaptive threshold pipeline
        img_mask = local_adaptive_thresholding(
            img, adaptive_threshold, adaptive_sigma, adaptive_sigma_bkgr
        )

        # remove holes
        img_mask_filled = binary_fill_holes(img_mask)

        # connected components
        img_seg_adaptive = measure.label(img_mask_filled)

        # split merged objects
        img_watershed = watershed_split(img_seg_adaptive, watershed_sigma)

        # remove small objects
        res = morphology.remove_small_objects(img_watershed, min_size=object_min_size)

        return res

    def segment_nuclei_movie(mov, selected_params):
        """Segment all frames of *mov* using previously found parameters for segment_nuclei function"""
        # create empty that will contain the segmented frames
        nuclei_segmented = []
        # loop and segment each frame in a loop
        for img in mo.status.progress_bar(mov, title='Segmenting..', completion_title='All frames segmented'):
            # here we have by default the same parameter values as before, if you have changed the above, update them here as well!
            res = segment_nuclei(
                img,
                adaptive_threshold=selected_params['Adaptive threshold'],
                adaptive_sigma=selected_params['Adaptive sigma'],
                adaptive_sigma_bkgr=selected_params['Adaptive sigma bkgr'],
                watershed_sigma=selected_params['Watershed sigma'],
                object_min_size=selected_params['Object min size'],
            )
            # each segmented frame, add it to the list
            nuclei_segmented.append(res)

        return np.stack(nuclei_segmented)  # convert list to a 3D array (of TYX shape)
    return (
        get_position,
        local_adaptive_thresholding,
        segment_nuclei,
        segment_nuclei_movie,
        show_image,
        show_segmentation,
        watershed_split,
    )


@app.cell
def __():
    import marimo as mo
    import numpy as np
    import tifffile
    from matplotlib import pyplot as plt
    from skimage import filters, measure, morphology, segmentation, feature
    from scipy.ndimage import binary_fill_holes, distance_transform_edt
    import os
    return (
        binary_fill_holes,
        distance_transform_edt,
        feature,
        filters,
        measure,
        mo,
        morphology,
        np,
        os,
        plt,
        segmentation,
        tifffile,
    )


if __name__ == "__main__":
    app.run()
