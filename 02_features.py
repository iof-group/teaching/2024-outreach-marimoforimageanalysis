import marimo

__generated_with = "0.8.15"
app = marimo.App(width="medium")


@app.cell
def __(mo):
    mo.md(r"""# Load Data""")
    return


@app.cell
def __(mo):
    f = mo.ui.file_browser(multiple=False, label='Select label image').form()

    mo.md(
        f"""
        ## Load segmentation label images
        {f}
        """
    )
    return f,


@app.cell
def __(f, mo, tifffile):
    mo.stop(not f.value)

    labels = tifffile.imread(f.value[0].path)
    return labels,


@app.cell
def __(f, mo):
    mo.stop(not f.value)

    channel_selection = mo.ui.dropdown(options=['rfp', 'gfp'], value='rfp', label='Channel')
    position_selection = mo.ui.dropdown(options=['13', '37', '46'], value='13', label='Position')
    load_button = mo.ui.run_button(label='Load Images')

    mo.md(
        f"""
        ## Load raw images
        {mo.vstack([
            mo.hstack([channel_selection, position_selection], justify='space-around'),
            load_button.center()
        ])}
        """
    )
    return channel_selection, load_button, position_selection


@app.cell
def __(
    channel_selection,
    get_position,
    labels,
    load_button,
    mo,
    position_selection,
):
    mo.stop(not load_button.value)

    img = get_position(
        './data/', 
        position_selection.value,
        channel_selection.value,
        (1,labels.shape[0])
    )
    return img,


@app.cell
def __(load_button, mo):
    mo.stop(not load_button.value)

    features_list = mo.ui.multiselect([
            "perimeter",
            "area",
            "mean_intensity",
            "max_intensity",
            "min_intensity",
            "eccentricity",
            "equivalent_diameter_area",
            "solidity",
            "label"
    ], value=["label", "area", "mean_intensity"], label='Features to compute')

    features_button = mo.ui.run_button(label="Compute features")

    mo.md(
        f"""
        # Features
        ## Calculate features
        {mo.vstack([features_list, features_button]).center()}
        """
    )
    return features_button, features_list


@app.cell
def __(features_button, features_list, img, labels, measure, mo, pd):
    mo.stop(not features_button.value)

    all_dfs = []
    for frame in mo.status.progress_bar(range(img.shape[0]), title="Calculating features..", completion_title="All features calculated."):
        current_df = pd.DataFrame(measure.regionprops_table(img[frame], labels[frame], properties=features_list.value))
        current_df['frame'] = frame
        all_dfs.append(current_df)

    df = pd.concat(all_dfs, axis=0)
    return all_dfs, current_df, df, frame


@app.cell
def __(mo):
    mo.md(r"""## Display features""")
    return


@app.cell
def __(df, px):
    px.line(df, x='frame', y='mean_intensity', color='label')
    return


@app.cell
def __(df, px):
    px.line(df.groupby('frame').mean().reset_index(), x='frame', y='mean_intensity')
    return


@app.cell
def __(df, px):
    px.histogram(df, x='area', animation_frame='frame').update_yaxes(range=[-10, 150])
    return


@app.cell
def __(mo):
    mo.md(r"""## Dataframe exploration""")
    return


@app.cell
def __(df):
    df
    return


@app.cell
def __(df, mo):
    mo.ui.dataframe(df)
    return


@app.cell
def __():
    import marimo as mo
    import numpy as np  # the go-to package to deal with array data
    import pandas as pd
    import tifffile  # the best package to read/write TIFFs
    from skimage import measure
    import plotly.express as px

    def get_position(data_dir, pos_id, channel, t_range):
        """Read all images from *data_dir* for a specified *pos_id*, *channel* and time interval (*t_start*, *t_end*)"""
        mov = []
        for t in mo.status.progress_bar(range(*t_range), title=f"Load Position 00{pos_id}, channel {channel}"):
            img_t = tifffile.imread(
                f"{data_dir}/00{pos_id}/tubulin_P00{pos_id}_T00{t:03d}_C{channel}_Z1_S1.tif"
            )
            mov.append(img_t)

        return np.stack(mov)
    return get_position, measure, mo, np, pd, px, tifffile


if __name__ == "__main__":
    app.run()
