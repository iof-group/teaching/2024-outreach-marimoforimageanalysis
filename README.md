# Marimo for image analysis
## *How to quickly convert Python notebooks into user-ready interfaces*

A brief illustration of how [Marimo](https://marimo.io/) can be useful for image analysis pipelines and user-ready deliverable applications.

The overall analysis pipeline is taken from our intro to Python for Image Analysis course: [PyxelPerfect](https://git.ista.ac.at/iof-group/teaching/2024-outreach-pyxelperfect).

### How to use
1. Create virtual environment
```bash
$ conda env create -f env.yml
```

2. Activate environment
```bash
$ conda activate marimo
```

3. Start up Marimo interface in **edit** or **app** mode:
```bash
$ marimo edit 01_segmentation.py
# OR
$ marimo run 01_segmentation.py
```

### Author
Marco Dalla Vecchia, Imaging and Optics Facility [IOF@ist.ac.at](IOF@ist.ac.at) at Institute of Science and Technology Austria (ISTA)


### DATA: Chromatin + Microtubles
[**H2B-mCherry (red) + mEGFP-α-Tubulin (green)**](https://cellcognition-project.org/demo_data.html)

Human Hela Kyoto cells stably expressing fluorescent markers of chromatin (histone-2b, H2B) and alpha-Tubulin. Data acquired by Katja Beck (Kutay group, ETH Zurich) and Michael Held (Gerlich group, ETH Zurich) at a Molecular Devices ImageXpress Micro.

Widefield epifluorescence, 20x dry objective (0.3225 µm / pixel), 1392x1040 pixel, 206 frames x 4.6 min = 15.8 h